import 'dart:io';
import 'Game.dart';

class Board {
  Game game = new Game();
  late String placeCardInGame = "";
  int turn = 1;

  board() {}

  showWelcome() {
    print("=== Welcome to Exploding Kitten game ===");
    print("get defuse a bomb card");
    showCard();
    print("random card in deck");
    print("deal out cards…");
  }

  //card on player hand
  showCard() {
    List lst = game.getPlayer();
    print("your card: $lst");
  }

  switchPlayer() {
    if (game.win != 0) {
      return game.getResult();
    }
    if (turn == 1) {
      turn = 2;
    } else {
      turn = 1;
      showPlayer();
      showCard();
    }
  }

  showPlayer() {
    if (game.win != 0) {
      return game.getResult();
    }
    if (turn == 1 && placeCardInGame == "") {
      print("-----your turn-----");
    } else if (turn == 2 && placeCardInGame == "attack") {
      print("-----bot turn-----");
      sleep(const Duration(seconds: 1));
      game.checkPlaceCard("attack");
    } else if (turn == 2) {
      print("-----bot turn-----");
      sleep(const Duration(seconds: 1));
      game.turnBot();
    }
    placeCardInGame = "";
    switchPlayer();
  }

  inputCard() {
    //player
    if (game.win != 0) {
      return game.getResult();
    }
    placeCardInGame = stdin.readLineSync()!;
    if (placeCardInGame == "attack") {
      showPlayer();
      inputCard();
      return;
    }
    game.checkPlaceCard(placeCardInGame);
    showCard();
  }
}
