import 'dart:io';
import 'Bot.dart';
import 'Card.dart';
import 'Player.dart';

class Game {
  int win = 0;
  List deck = Card().Deck();
  List player = Player().player();
  List bot = Bot().bot();
  dealOutCard() {
    randomCard();
    for (int i = 0; i < 5; i++) {
      player.add(deck.removeAt(0));
      bot.add(deck.removeAt(0));
    }
    add();
    bot.shuffle();
  }

  add() {
    for (int i = 1; i <= 4; i++) {
      deck.add('exploding kitten');
    }
    randomCard();
  }

  getPlayer() {
    return this.player;
  }

  getBot() {
    return this.bot;
  }

  randomCard() {
    deck.shuffle();
  }

  checkPlaceCard(String card) {
    //Player
    player.remove(card);
    switch (card) {
      case "shuffle":
        randomCard();
        break;
      case "attack":
        return attack("b");
      case "help":
        player.add(bot.first);
        bot.remove(bot.first);
        break;
      case "see the future":
        return future();
      case "exploding kitten":
        return checkGetCard();
      case "draw card":
        return checkGetCard();
      case "draw":
        return checkGetCard();
    }
  }

  checkGetCard() {
    //Player
    if (deck.first == "exploding kitten" && player.contains('defuse') == true) {
      print("! ! ! exploding kitten ! ! !");
      print("defuse");
      player.remove("defuse");
      randomCard();
    } else if (deck.first == "exploding kitten" &&
        player.contains('defuse') == false) {
      print("! ! ! exploding kitten ! ! !");
      print("You don't have defuse!!!");
      win = 2;
    } else {
      player.add(deck.first);
    }
    removeCard();
  }

  turnBot() {
    //checkPlaceCardBot & select to place card
    if (deck.first == "exploding kitten" && bot.contains("defuse") == true) {
      print("draw card");
      sleep(const Duration(seconds: 1));
      print("! ! ! exploding kitten ! ! !");
      sleep(const Duration(seconds: 1));
      print("defuse");
      bot.remove("defuse");
      randomCard();
      return;
    } else if (deck.first == "exploding kitten" &&
        bot.contains("defuse") == false) {
      print("draw card");
      sleep(const Duration(seconds: 1));
      print("! ! ! exploding kitten ! ! !");
      sleep(const Duration(seconds: 1));
      print("Bot don't have defuse!!!");
      win = 1;
      return;
    } else if (bot.isEmpty || bot.first == "defuse") {
      bot.shuffle();
      return botDrawCard();
    } else if (bot.first == "skip") {
      print("skip");
      bot.remove("skip");
    } else {
      print(bot.first);

      if (bot.first == "attack") {
        print("-----your turn-----");
        checkBotCard(bot.first);
        bot.remove(bot.first);
        sleep(const Duration(seconds: 1));
        print("-----bot turn-----");
        turnBot();
        return;
      }
      checkBotCard(bot.first);
      bot.remove(bot.first);
      sleep(const Duration(seconds: 1));
      botDrawCard();
      return;
    }
  }

//Bot place card
  checkBotCard(String card) {
    switch (card) {
      case "shuffle":
        randomCard();
        break;
      case "attack":
        return attack("p");
      case "help":
        bot.add(player.first);
        player.remove(player.first);
        break;
      default:
        return;
    }
  }

  botDrawCard() {
    print("draw card");
    bot.add(deck.first);
    removeCard();
  }

  getResult() {
    if (win == 1) {
      print("=====You Win!=====");
    } else if (win == 2) {
      print("=====Bot Win!=====");
    }
  }

  future() {
    print(deck.getRange(0, 3));
  }

  attack(String A) {
    //player attack bot
    if (A == "b") {
      for (int i = 1; i <= 2; i++) {
        if (deck.first != "exploding kitten") {
          sleep(const Duration(seconds: 1));
          botDrawCard();
        } else {
          turnBot();
        }
      }
      getPlayer();
    } else {
      for (int i = 1; i <= 2; i++) {
        print("draw card");
        sleep(const Duration(seconds: 1));
        checkPlaceCard("draw card");
      }
      sleep(const Duration(seconds: 1));
    }
  }

  void removeCard() {
    if (deck.first != "exploding kitten") {
      deck.remove(deck.first);
    } else {
      randomCard();
    }
  }
}
