import 'Board.dart';

void main(List<String> args) {
  Board board = new Board();
  board.showWelcome();
  board.game.dealOutCard();
  board.showPlayer();
  board.showCard();

  while (board.game.win == 0) {
    while (board.placeCardInGame != 'draw card' &&
        board.placeCardInGame != 'skip' &&
        board.placeCardInGame != 'draw') {
      board.inputCard();
    }
    board.showPlayer();
  }
}
